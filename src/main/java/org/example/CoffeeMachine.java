package org.example;

import org.example.coffee.Cappuchino;
import org.example.coffee.Coffee;
import org.example.coffee.CoffeeMaker;
import org.example.customer.Customer;
import org.example.customer.Customer1;

public class CoffeeMachine {
    public static void main(String[] args) {
//        Condiment milk = new Milk();
//        Condiment honey = new Honey();
//        Condiment moka = new Moka();
//        Coffee coffeeWithCondiments = new CoffeeWithCondiments(milk,honey);
//        Customer customer1 = new Customer1();
//        var coffeeMaker = new CoffeeMaker(coffeeWithCondiments,customer1);
//
//        coffeeMaker.pour();
//
//        coffeeWithCondiments.displayCoffeeName();
//        System.out.println();
//        System.out.print("Cost Of A Coffee : ");
//        coffeeWithCondiments.displayCost();
//        coffeeMaker.getChange();


        Coffee capuchino = new Cappuchino();
        Customer customer1 = new Customer1();
        var coffeeMaker = new CoffeeMaker(capuchino,customer1);

        coffeeMaker.pour();

        capuchino.displayCoffeeName();
        System.out.println();
        System.out.print("Cost Of A Coffee : ");
        System.out.println(capuchino.displayCost());
        coffeeMaker.getChange();

    }
}
