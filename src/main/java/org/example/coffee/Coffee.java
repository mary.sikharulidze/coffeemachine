package org.example.coffee;

public interface Coffee {
    double displayCost();
    void displayCoffeeName();
}
