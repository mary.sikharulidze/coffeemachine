package org.example.coffee;

import org.example.condiment.Condiment;

public class CoffeeWithCondiments implements Coffee{

    private final Condiment[] condiments;
    public double cost = 5;

    public CoffeeWithCondiments(Condiment... condiments){
        this.condiments = condiments;
        for (Condiment condiment:condiments ) {
            cost += condiment.getCost();
        }
    }

    @Override
    public double displayCost() {
        return cost;
    }

    @Override
    public void displayCoffeeName() {
        System.out.print( "Coffee with Condiments : ");
        for (Condiment condiment:condiments) {
            System.out.print(condiment.displayCondimentName());
        }
    }

}
