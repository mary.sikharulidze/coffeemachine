package org.example.coffee;

import org.example.condiment.Condiment;
import org.example.condiment.Milk;

public class Cappuchino implements Coffee{

    double cost = 5;

    public Cappuchino(){
        Condiment milk = new Milk();
        cost += milk.getCost()*2;
    }

    @Override
    public double displayCost() {
        return cost;
    }

    @Override
    public void displayCoffeeName() {
        System.out.print( "Cappuchino");
    }
}
