package org.example.coffee;

import org.example.customer.Customer;

public class CoffeeMaker {
    Coffee coffeeWithCondiments;
    Customer customer1;
    private double change;

    public void pour() {
        if (customer1.getMoney() >= coffeeWithCondiments.displayCost()) {
            System.out.println("Pouring Coffee...");
        } else throw new IllegalArgumentException("Not Enough Money");
    }
    public void getChange(){
        if (customer1.getMoney() >= coffeeWithCondiments.displayCost()) {
            change = customer1.getMoney() - coffeeWithCondiments.displayCost();
            System.out.println("Change : "+change);
        }
    }
    public CoffeeMaker(Coffee coffee, Customer customer1) {
        this.coffeeWithCondiments =  coffee;
        this.customer1 = customer1;
    }
}
