package org.example.condiment;

public class Moka implements Condiment {
    private double cost = 2.2;

    @Override
    public double getCost() {
        return cost;
    }

    @Override
    public String displayCondimentName() {
        return "Moka ";
    }

}
