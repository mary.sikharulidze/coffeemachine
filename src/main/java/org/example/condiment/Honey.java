package org.example.condiment;

public class Honey implements Condiment {
    private double cost = 2;

    @Override
    public double getCost() {
        return cost;
    }

    @Override
    public String displayCondimentName() {
        return "Honey ";
    }

}
