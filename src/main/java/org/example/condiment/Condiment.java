package org.example.condiment;

public interface Condiment {
    double cost=0;
    double getCost();
    String displayCondimentName();

}
