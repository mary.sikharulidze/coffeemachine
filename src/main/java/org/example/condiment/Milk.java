package org.example.condiment;

public class Milk implements Condiment{

    private double cost=1;

    @Override
    public double getCost() {
         return cost;
    }

    @Override
    public String displayCondimentName() {
        return "Milk ";
    }

}
