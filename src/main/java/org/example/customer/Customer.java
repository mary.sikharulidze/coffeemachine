package org.example.customer;

public interface Customer {
    double money = 0;
    double getMoney();
}
